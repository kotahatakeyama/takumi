<?php
//session_cache_limiter('public');
session_start();
ini_set("mbstring.detect_order","UTF-8,auto");
ini_set("mbstring.http_input","auto");
ini_set("mbstring.internal_encoding","UTF-8");
ini_set("mbstring.encoding_translation","off");
ini_set("mbstring.http_output","UTF-8");
ini_set("mbstring.language","japanese");
//error_reporting(0);
// ブラウザから直接のアクセスを禁止する
if($access_check!=true){exit();}
$_SESSION['is_mobile'] = false;
//////////////////////////////////////////////////////////////////////////////
//
// EFO用ライブラリファイル
//
// @charset utf-8
// @author tsuji@syspla.net
// @revision 6
//
//
// --設定目次--
//
// 1, 入力フィルター
// 2, 必須項目設定
// 3, 最大文字数設定
// 4, 最小文字数設定
// 5, バリデーション設定
// 6, その他（保存、メールなど）
//
//
///////////////////////////////////////////////////////////////////////////////


// 1, 入力フィルター //////////////////////////////////////////////////////////
/*
入力値にフィルターをかけ、変換等処理する
▼指定可能なもの一覧
A: スペース削除
B: 全角→半角
C: 半角→全角
D: ハイフン削除
例）
$input_filter['email'] = 'AB';//スペース削除、全角→半角
$input_filter['address1'] = 'AC';//スペース削除、半角→全角
*/
$input_filter = array();
$input_filter['email'] = 'AB';
//$input_filter['email_2'] = 'AB';


// 2, 必須項目設定 ////////////////////////////////////////////////////////////
/*
必須項目とエラーメッセージの設定
例）
$must['name'] = "氏名が入力されていません。";
$must['kana'] = "氏名(カナ)が入力されていません。";
$must['privacy'] = "個人情報保護方針への同意がありません。";
*/
$must = array();
$must['name'] = "名前が入力されていません。";
// $must['huri'] = "ふりがなが入力されていません。";
// $must['tel'] = "電話番号が入力されていません。";
$must['email'] = "メールアドレスが入力されていません。";

// 3, 最大文字数設定 //////////////////////////////////////////////////////////
/*
入力値としての最大値とエラーメッセージを設定
　テキスト入力系では文字数
　チェックボックス（複数選択）の場合はチェックの数となります
例）
$max_input['name'] = array(50, "50文字以内で入力してください。");
$max_input['checkbox'] = array(3,"3つまでチェック可能");
*/
$max_input = array();
// $max_input['name'] = array(100, "入力された名前が長すぎます。");
// $max_input['huri'] = array(100, "入力されたふりがなが長すぎます。");
// $max_input['company'] = array(100, "入力された電話番号が長すぎます。");
// $max_input['job'] = array(100, "入力された電話番号が長すぎます。");
// $max_input['tel'] = array(20, "入力された電話番号が長すぎます。");
// $max_input['email'] = array(256, "入力されたメールアドレスが長すぎます。");
// $max_input['email_2'] = array(256, "入力されたメールアドレス確認用が長すぎます。");


// 4, 最小文字数設定 //////////////////////////////////////////////////////////
/*
入力値としての最小値とエラーメッセージを設定
　テキスト入力系では文字数
　チェックボックス（複数選択）の場合はチェックの数となります
例）
$min_input['email'] = array(5,"メールアドレスが短すぎます。");
$min_input['zip'] = array(7,"郵便番号が短すぎます。");
*/
$min_input = array();


// 5, バリデーション設定 //////////////////////////////////////////////////////
/*
入力値のチェックとエラーメッセージを設定
▼指定可能なもの一覧
A: 数字、記号以外
B: メールアドレス
C: 数字（プラスハイフン）のみ
D: ひらがな、数字、記号以外
例）
$validation['name'] = array('A', "数字、記号は使用できません。");
$validation['email'] = array('B', "メールアドレスに使用できない文字が含まれています。");
*/
$validation = array();

/*
一致させるバリデーション
例）
$match['email'] = array('email1', "メールアドレスが一致しません。");// "email" と "email1" の入力が一致する必要がある
*/
$match = array();
// $match['email'] = array('email2', "メールアドレスが一致しません。");



// 6, その他 ///////////////////////////////////////////////////////////////////
/*
メールを送信する場合のテンプレートはmailフォルダ内
csv保存する場合、WEBから見られないように.htaccessでアクセス制限したほうがいいと思います
ユーザーのメールアドレスはname="email"で入力している必要があります。
*/
$user_mail = "info@tohokuitfactory.com";//ユーザーへ送信する場合、"FROMの"アドレス入力　空欄で送信しない
$admin_mail= "all@tohokuitfactory.com";//管理者へ送信する場合、アドレス入力　空欄で送信しない
$csv_file = "csv/data.csv";//CSV保存する場合はファイルパス（パーミッション666）　空欄で保存しない





$validation_number = '0123456789-';//数値（プラスハイフン）に使われる文字
$validation_kigou = "'".'@!"#$%&()*+,-./:;<=>?[\]^_{|}~！”＃＄％＆’（）＝～｜‘｛＋＊｝＜＞？＿－＾￥＠「；：」、。・';//記号に使われる文字

/************************** -- 設定ここまで-- *********************************/

























$_POST = jGetInputData('post');
function jGetInputData($type = 'post', $str = null, $del_null_byte = true) {
  if ($str === null) {
  switch (strtolower($type)) {
    case 'get': // GET の生データを取得
    $str = $_SERVER["QUERY_STRING"];
    break;
    case 'post': // POST の生データを取得
    default:
    $str = file_get_contents("php://input");
  }
  }

  if (!is_string($str)) return false;
  if (trim($str) === '') return array(); // 何もなし

  // 「&」が含まれていれば分解
  $datas = (strpos($str, '&') === false) ? array($str) : explode('&', $str);
  $rtn = array();

  foreach($datas as $data) {
  $name = null;
  $val = null;

  if (strpos($data, '=') === false) {
    $name = urldecode($data);
  } else {
    $_tmp = explode('=', $data, 2);
    $name = urldecode($_tmp[0]);
    $val = urldecode($_tmp[1]);
  }

  if($_SESSION['is_mobile']){
    $val = mb_convert_encoding($val,"UTF-8","SJIS-win");
  }
  if (!isset($rtn[$name])) {
    // アイテム名が存在しなければそのまま代入
    $rtn[$name] = $val;
  } else {
    if (!is_array($rtn[$name])) {
    // アイテム名の変数が存在し、
    // アイテム名の配列が存在しなければ配列にする
    $rtn[$name] = array($rtn[$name]);
    }
    // 新しい値を配列に追加
    $rtn[$name][] = $val;
  }
  }

  if ($del_null_byte) {
  $rtn = jDelNullbyte($rtn);
  }

  return $rtn;
}

if(!function_exists('file_get_contents')){
  function file_get_contents($fname){
    if (!file_exists($fname)) return FALSE;
    if (!$fp = fopen('r', $fname)) return FALSE;
    $a = "";
    while (!feof($fp)) {
      $a .= fread($fp, 8192);
    }
    return $a;
  }
}

function jDelNullbyte($data) {
  if (is_array($data)) {
    foreach($data as $k => $v){
      $data[$k] = jDelNullbyte($v);
    }
    return $data;
  }

  return str_replace("\0", '', $data);
}

function procSave()
{
  global $user_mail;
  global $admin_mail;
  global $csv_file;

  $_SESSION['postval']['datetime'] = date('Y年m月d日 H時i分');
  $_SESSION['postval']['host'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);

  $filename = 'csv/counter.dat';
  $fp = fopen($filename, "r+");
  flock($fp, LOCK_EX);
  $count = fgets($fp,1024);
  $count = trim($count);
  $count++;
  fseek($fp, 0);
  fputs($fp, $count);
  flock($fp, LOCK_UN);
  fclose($fp);
  $_SESSION['postval']['count'] = sprintf("%05d",$count);

  if(!myempty($user_mail) && $_SESSION['postval']['email']){
    $file = file('mail/user.txt');
    $mtitle = $file[0];
    unset($file[0]);
    $contents = implode("",$file);
    foreach($_SESSION['postval'] as $k => $v){
      if(is_array($v)){
        $v = implode(", ",$v);
      }
      $contents = str_replace('{'.$k.'}',$v,$contents);
    }
    $contents = preg_replace("|{\w+}|mu","",$contents);
    $mtitle = mb_convert_encoding($mtitle,"JIS","UTF-8");
    $mtitle = "=?iso-2022-jp?B?" . base64_encode($mtitle) ."?=";
    $mbody = mb_convert_encoding($contents,"JIS","UTF-8");
    mail($_SESSION['postval']['email'],$mtitle,$mbody,"Content-Type: text/plain; charset=\"ISO-2022-JP\";\r\n".'From: '.$user_mail);
  }
  if(!myempty($admin_mail)){
    $file = file('mail/admin.txt');
    $mtitle = $file[0];
    unset($file[0]);
    $contents = implode("",$file);
    foreach($_SESSION['postval'] as $k => $v){
      if(is_array($v)){
        $v = implode(", ",$v);
      }
      $contents = str_replace('{'.$k.'}',$v,$contents);
    }
    $contents = preg_replace("|{\w+}|mu","",$contents);
    $mtitle = mb_convert_encoding($mtitle,"JIS","UTF-8");
    $mtitle = "=?iso-2022-jp?B?" . base64_encode($mtitle) ."?=";
    $mbody = mb_convert_encoding($contents,"JIS","UTF-8");
    $from = $admin_mail;
    if(isset($_SESSION['postval']['email']) && !myempty($_SESSION['postval']['email'])){
      $from = $_SESSION['postval']['email'];
    }
    $from = $user_mail;
    mail($admin_mail,$mtitle,$mbody,"Content-Type: text/plain; charset=\"ISO-2022-JP\";\r\n".'From: '.$from);
  }
  if(!empty($csv_file)){
    $csv = "";
    $arr = array('count','page','page_non','year','month','day','date_non','design','detail','rwd','js','cms','print','name','kana','cms','companyName','zip','pref','town','buil','tel','mail','datetime','host');
    foreach($arr as $key){
      $v = $_SESSION['postval'][$key];
      if(is_array($v)){
        $v = implode(", ",$v);
      }
      $v = str_replace("\r\n","\r",$v);
      $csv .= '"'.$v.'",';
    }
    $csv .= "\r\n";
    $csv = mb_convert_encoding($csv,"SJIS-win","UTF-8");
    $fp = fopen($csv_file, "a");
    if(flock($fp, LOCK_EX)){
      fwrite($fp,$csv);
      fclose($fp);
    } else {
      $body = date('Y-m-d H:i:s')."\n";
      $body .= mb_convert_encoding($csv,"UTF-8",'SJIS-win');
      mail('shoji@tohokuitfactory.com','FILE Lock Err:'.$csv_file,$body);
    }
  }
}

function getTemplateType(){
  $ua = $_SERVER['HTTP_USER_AGENT'];
  if (preg_match("/^DoCoMo/i", $ua)) { // DoCoMo
    $_SESSION['is_mobile'] = true;
    header('Content-Type: text/html; charset=Shift-JIS');
    return "feature";
  } else if (preg_match("/^SoftBank/i", $ua)) { // SoftBank
    $_SESSION['is_mobile'] = true;
    header('Content-Type: text/html; charset=Shift-JIS');
    return "feature";
  } else if (preg_match("/^KDDI\-/i", $ua)) { // au (XHTML)
    $_SESSION['is_mobile'] = true;
    header('Content-Type: text/html; charset=Shift-JIS');
    return "feature";
  } else if (preg_match("/UP\.Browser/i", $ua)) { // au (HDML) TU-KA
    $_SESSION['is_mobile'] = true;
    header('Content-Type: text/html; charset=Shift-JIS');
    return "feature";
  } else if (preg_match("/WILLCOM/i", $ua)){ // WILLCOM
    $_SESSION['is_mobile'] = true;
    header('Content-Type: text/html; charset=Shift-JIS');
    return "feature";
  } else if (preg_match("/iPhone/i", $ua)) { // iPhone
    $_SESSION['is_mobile'] = false;
    header('Content-Type: text/html; charset=UTF-8');
    return "mobile";
  } else if (preg_match("/Android/i", $ua)) { // Android
    $_SESSION['is_mobile'] = false;
    header('Content-Type: text/html; charset=UTF-8');
    return "mobile";
  } else {
    $_SESSION['is_mobile'] = false;
    header('Content-Type: text/html; charset=UTF-8');
    return "pc";
  }
}

function getEfoJs()
{
  global $must;
  global $max_input;
  global $min_input;
  global $validation;
  global $validation_kigou;
  global $validation_number;
  global $match;

  //jquery必須
  $out = "";
  $out .="
  jQuery(function(){
    jQuery('input[type=text]').keyup(function(){
      checkSubmit(this.parentNode,false);
    });
    jQuery('input[type=text]').blur(function(){
      checkSubmit(this.parentNode,false);
    });
    jQuery('textarea').keyup(function(){
      checkSubmit(this.parentNode,false);
    });
    jQuery('textarea').blur(function(){
      checkSubmit(this.parentNode,false);
    });
    jQuery('#birthday').change(function(){
      if(!jQuery('#birthyear').val()){
        return true;
      }
      if(!jQuery('#birthmonth').val()){
        return true;
      }
      var d = jQuery('#birthyear').val();
      d += '/';
      if(jQuery('#birthmonth').val()<10){
        d += '0'+jQuery('#birthmonth').val();
      } else {
        d += jQuery('#birthmonth').val();
      }
      d += '/';
      if(jQuery('#birthday').val()<10){
        d += '0'+jQuery('#birthday').val();
      } else {
        d += jQuery('#birthday').val();
      }
      $('.error .tooltip').stop(true,true).hide();
      $('.error').removeClass('error');
      if(!ckDate(d)){
        $(this).parents('.row').addClass('newerror').find('.tooltip > p').text('日付を確認してください');
        $('.newerror').removeClass('newerror').addClass('error');
        $('.error .tooltip').fadeIn(1000).delay(5000).fadeOut(1000);
      }
    });
    jQuery('#birthmonth').change(function(){
      if(!jQuery('#birthyear').val()){
        return true;
      }
      if(!jQuery('#birthday').val()){
        return true;
      }
      var d = jQuery('#birthyear').val();
      d += '/';
      if(jQuery('#birthmonth').val()<10){
        d += '0'+jQuery('#birthmonth').val();
      } else {
        d += jQuery('#birthmonth').val();
      }
      d += '/';
      if(jQuery('#birthday').val()<10){
        d += '0'+jQuery('#birthday').val();
      } else {
        d += jQuery('#birthday').val();
      }
      $('.error .tooltip').stop(true,true).hide();
      $('.error').removeClass('error');
      if(!ckDate(d)){
        $(this).parents('.row').addClass('newerror').find('.tooltip > p').text('日付を確認してください');
        $('.newerror').removeClass('newerror').addClass('error');
        $('.error .tooltip').fadeIn(1000).delay(5000).fadeOut(1000);
      }
    });
    jQuery('#birthyear').change(function(){
      if(!jQuery('#birthmonth').val()){
        return true;
      }
      if(!jQuery('#birthday').val()){
        return true;
      }
      var d = jQuery('#birthyear').val();
      d += '/';
      if(jQuery('#birthmonth').val()<10){
        d += '0'+jQuery('#birthmonth').val();
      } else {
        d += jQuery('#birthmonth').val();
      }
      d += '/';
      if(jQuery('#birthday').val()<10){
        d += '0'+jQuery('#birthday').val();
      } else {
        d += jQuery('#birthday').val();
      }
      $('.error .tooltip').stop(true,true).hide();
      $('.error').removeClass('error');
      if(!ckDate(d)){
        $(this).parents('.row').addClass('newerror').find('.tooltip > p').text('日付を確認してください');
        $('.newerror').removeClass('newerror').addClass('error');
        $('.error .tooltip').fadeIn(1000).delay(5000).fadeOut(1000);
      }
    });
  });
  ";
  $out .="
function checkSubmit(f,confirm){
  var chk,ret=true;
  $('.error .tooltip').stop(true,true).hide();
  $('.error').removeClass('error');
  ";
  foreach($must as $name => $errmsg){
    $out .= "
    chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
    if(chk.length){
      if( ((chk.attr('type')=='radio' || chk.attr('type')=='checkbox') && chk.filter(':checked').length == 0) || chk.val()==''){
        chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($errmsg)."');
        ret=false;
      }
    }
    ";
  }
  foreach($max_input as $name => $arr){
    $out .="
    chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
    if(chk.length){
      if( (chk.attr('type')=='checkbox' && chk.filter(':checked').length > ".$arr[0].") || (chk.val()).length > ".$arr[0]."){
        chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
        ret=false;
      }
    }
    ";
  }
  foreach($min_input as $name => $arr){
    $out .="
    chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
    if(chk.length){
      if( (chk.attr('type')=='checkbox' && chk.filter(':checked').length > 0 && chk.filter(':checked').length < ".$arr[0].") ||
          (chk.attr('type')!='checkbox' && (chk.val()).length > 0 && (chk.val()).length < ".$arr[0].") ){
        chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
        ret=false;
      }
    }
    ";
  }
  foreach($validation as $name => $arr){
    switch($arr[0]){
    case 'A'://数字、記号以外
      $out .="
      chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
      if(chk.length){
        if( (chk.val()).length > 0 && (chk.val()).match(/[".preg_quote($validation_kigou, '/')."]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        } else if( (chk.val()).length > 0 && (chk.val()).match(/[".preg_quote($validation_number, '/')."]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        }
      }
      ";
      break;
    case 'B'://メールアドレス
      $out .="
      chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
      if(chk.length){
        if( (chk.val()).length > 0 && !(chk.val()).match(/^[\w!#$%&'*+\/=?^_{}\\|~\.\-@]+$/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        }
      }
      ";
      break;
    case 'C'://数字のみ
      $out .="
      chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
      if(chk.length){
        if( (chk.val()).length > 0 && !(chk.val()).match(/^[".preg_quote($validation_number, '/')."]+$/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        }
      }
      ";
      break;
    case 'D'://ひらがな、数字、記号以外
      $out .="
      chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
      if(chk.length){
        if( (chk.val()).length > 0 && (chk.val()).match(/[".preg_quote($validation_kigou, '/')."]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        } else if( (chk.val()).length > 0 && (chk.val()).match(/[".preg_quote($validation_number, '/')."]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        } else if( (chk.val()).length > 0 && (chk.val()).match(/[ぁ-ゞ]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        } else if( (chk.val()).length > 0 && (chk.val()).match(/[a-zA-Z]/) ){
          chk.parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
          ret=false;
        }
      }
      ";
      break;
    }
  }
  foreach($match as $name => $arr){
    $out .="
    chk = jQuery('*[name=\"".htmlspecialchars($name)."\"]', f);
    if(chk.length){
      if( (chk.val()).length > 0 && chk.val() != jQuery('*[name=\"".htmlspecialchars($arr[0])."\"]', f).val() ){
        jQuery('*[name=\"".htmlspecialchars($arr[0])."\"]', f).parents('.row').addClass('newerror').find('.tooltip > p').text('".htmlspecialchars($arr[1])."');
        ret=false;
      }
    }
    ";
  }

  $out .="
  $('.newerror').removeClass('newerror').addClass('error');
  if(ret==false){
    //window.scrollTo(0,0);
    $('.error .tooltip').fadeIn(1000).delay(5000).fadeOut(1000);
  }
  return ret;
}
  ";
  $out .= "

  jQuery.fn.zip2addr = function(target){
    var c = {
    api: location.protocol + '//www.google.com/transliterate?langpair=ja-Hira|ja&jsonp=?',
    prefectureToken: '(東京都|道|府|県)',
    zipDelimiter: '-'
    }

  var cache = $.fn.zip2addr.cache;

    var getAddr = function(zip,callback){
        $.getJSON(c.api,{'text':zip},
      function(json){
        if(RegExp(c.prefectureToken).test(json[0][1][0])){
          callback(json[0][1][0].replace(RegExp('(.*?'+c.prefectureToken+')(.+)'),function(a,b,c,d){return [b,d]}))
        }
      }
    )
  }

  var fillAddr = (function(){
    if(typeof target == 'object' && target.pref){
      return function(addr){
        var addrs = addr.split(',');
        if(addrs){
          if(!RegExp(addrs[1]).test($(target.addr).val())){
            $(target.pref).val(addrs[0]);
            $(target.addr).val(addrs[1]);
          }
        }else if(!RegExp(addrs[1]).test($(target.addr).val())){
          $(target.pref).add(target.addr).val('');
        }
      }
    }else{
      return function(addr){
        var addrStr = addr.replace(',','');
        var addrField = target.addr || target;
        if(addrStr){
          if(!RegExp(addrStr).test($(addrField).val())){
            $(addrField).val(addrStr);
          }
        }else if(!RegExp(addrStr).test($(addrField).val())){
          $(addrField).val('');
        }
      }
    }
  })()

  //From http://liosk.blog103.fc2.com/blog-entry-72.html
  var fascii2ascii = (function() {
    var pattern = /[\uFF01-\uFF5E]/g, replace = function(m) {
      return String.fromCharCode(m.charCodeAt() - 0xFEE0);
    };
    return function(s){return s.replace(pattern, replace);};
  })();

  var check = function(_val){
    var val = fascii2ascii(_val).replace(/\D/g,'');
    if(val.length == 7){
      if(cache[val] == undefined){
        getAddr(val.replace(/(\d\d\d)(\d\d\d\d)/,'$1-$2'),function(json){
          cache[val] = json;
          fillAddr(json);
        })
      }else{
        fillAddr(cache[val]);
      }
    }
  }

    this.each(function(){
        var elem = $(this);
    if(typeof target == 'object' && target.zip2){
      elem.add($(target.zip2))
        .bind('keyup.zip2addr change.zip2addr',function(){
          check(elem.val()+''+$(target.zip2).val())
        })
        .bind('blur.zip2addr',function(){
          $(this).val(function(){
            return fascii2ascii($(this).val())
          })
        })
    }else{
      elem
        .bind('keyup.zip2addr change.zip2addr',function(){
          check(elem.val())
        })
        .bind('blur.zip2addr',function(){
          $(this).val(function(){
            return fascii2ascii($(this).val()).replace(/\D/g,'').replace(/(\d\d\d)(\d\d\d\d)/,'$1'+c.zipDelimiter+'$2')
          })
        })
    }
    });

    return this;
};

jQuery.fn.zip2addr.cache = {};

jQuery(function(){
  jQuery('#zip1').zip2addr({
    zip2:'#zip2',
    pref:'#pref',
    addr:'#address1'
  });
  jQuery('#zip').zip2addr({
    pref:'#pref',
    addr:'#address1'
  });
});

function ckDate(datestr) {
    // 正規表現による書式チェック
    if(!datestr.match(/^\d{4}\/\d{2}\/\d{2}$/)){
        return false;
    }
    var vYear = datestr.substr(0, 4) - 0;
    var vMonth = datestr.substr(5, 2) - 1; // Javascriptは、0-11で表現
    var vDay = datestr.substr(8, 2) - 0;
    // 月,日の妥当性チェック
    if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        var vDt = new Date(vYear, vMonth, vDay);
        if(isNaN(vDt)){
            return false;
        }else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
";
  return $out;
}

function echoVal($name)
{
  if(isset($_SESSION['postval'][$name])){
    $ret = $_SESSION['postval'][$name];
  } else {
    $ret = "";
  }
  if(is_array($ret)){
    $ret = implode(", ",$ret);
  }
  $ret = htmlspecialchars($ret);
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo $ret;
}

function echoTextarea($name)
{
  if(isset($_SESSION['postval'][$name])){
    $ret = $_SESSION['postval'][$name];
  } else {
    $ret = "";
  }
  $ret = htmlspecialchars($ret);
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo nl2br($ret);
}

function echoOptions($name,$arg1,$arg2=null)
{
  $ret = "";
  if($arg2){
    for($i=$arg1;$i<=$arg2;$i++){
      if(isset($_SESSION['postval'][$name]) && $_SESSION['postval'][$name] == $i){
        $ret .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';
      } else {
        $ret .= '<option value="'.$i.'">'.$i.'</option>';
      }
    }
  } else if(is_array($arg1)){
    foreach($arg1 as $k => $i){
      if($_SESSION['is_mobile']){
        $i = mb_convert_encoding($i,"UTF-8","SJIS-win");
      }
      if(isset($_SESSION['postval'][$name]) && $_SESSION['postval'][$name] == $i){
        $ret .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';
      } else {
        $ret .= '<option value="'.$i.'">'.$i.'</option>';
      }
    }
  }
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo $ret;
}

function echoPrefOptions($name)
{
  $todofuken = array("北海道","青森県","岩手県","宮城県","秋田県","山形県","福島県", "茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県", "新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県","静岡県","愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県","鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県","高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県");
  $ret = "";
  foreach($todofuken as $k => $i){
    if(isset($_SESSION['postval'][$name]) && $_SESSION['postval'][$name] == $i){
      $ret .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';
    } else {
      $ret .= '<option value="'.$i.'">'.$i.'</option>';
    }
  }
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo $ret;
}

function echoChecked($name, $val, $is_default=0)
{
  if($_SESSION['is_mobile']){
    $val = mb_convert_encoding($val,"UTF-8","SJIS-win");
  }
  $ret = "";
  if(isset($_SESSION['postval'][$name])){
    if(is_array($_SESSION['postval'][$name]) && array_search($val, $_SESSION['postval'][$name])!==FALSE){
      $ret = ' checked="checked"';
    } else if($_SESSION['postval'][$name] == $val){
      $ret = ' checked="checked"';
    }
  } else if($is_default){
    $ret = ' checked="checked"';
  }
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo $ret;
}

function echoSelected($name, $val, $is_default=0)
{
  if($_SESSION['is_mobile']){
    $val = mb_convert_encoding($val,"UTF-8","SJIS-win");
  }
  $ret = "";
  if(isset($_SESSION['postval'][$name])){
    if(is_array($_SESSION['postval'][$name]) && array_search($val, $_SESSION['postval'][$name])!==FALSE){
      $ret = ' selected="selected"';
    } else if($_SESSION['postval'][$name] == $val){
      $ret = ' selected="selected"';
    }
  } else if($is_default){
    $ret = ' selected="selected"';
  }
  if($_SESSION['is_mobile']){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
  }
  echo $ret;
}

function echoError($v1,$v2=null,$v3=null)
{
  if(isset($_SESSION['errors'][$v1])){
    echo "error"; return;
  }
  if($v2 && isset($_SESSION['errors'][$v2])){
    echo "error"; return;
  }
  if($v3 && isset($_SESSION['errors'][$v3])){
    echo "error"; return;
  }
}

function echoErrorMessage($v1,$v2=null,$v3=null)
{
  $ret = "";
  if(isset($_SESSION['errors'][$v1])){
    $ret .= $_SESSION['errors'][$v1];
  }
  if($v2 && isset($_SESSION['errors'][$v2])){
    $ret .= $_SESSION['errors'][$v2];
  }
  if($v3 && isset($_SESSION['errors'][$v3])){
    $ret .= $_SESSION['errors'][$v3];
  }
  $ret = htmlspecialchars($ret);
  $ret = nl2br($ret);
  if($_SESSION['is_mobile'] && $ret){
    $ret = mb_convert_encoding($ret,"SJIS-win","UTF-8");
    $ret = '<span style="color:red;">'.$ret.'</span><br>';
  }
  echo $ret;
}

function addErrorMessage($name, $errmes)
{
  if(!isset($_SESSION['errors'][$name])){
    $_SESSION['errors'][$name] = $errmes;
  } else {
    $_SESSION['errors'][$name] .= "\n".$errmes;
  }
}

function check_input_data()
{
  $_SESSION['errors'] = array();
  proc_input_filter();
  proc_must();
  proc_max();
  proc_min();
  proc_validate();
  $_SESSION['postval'] = $_POST;
  if(count($_SESSION['errors'])==0){
    return true;
  }
  return false;
}


function proc_validate()
{
  global $validation;
  global $validation_kigou;
  global $validation_number;
  global $match;
/*
A: 数字、記号以外
B: メールアドレス
C: 数字のみ
D: ひらがな、数字、記号以外
*/
  foreach($validation as $name => $arr){
    if(isset($_POST[$name]) && !myempty($_POST[$name])){
      switch($arr[0]){
      case 'A':
        if(preg_match("/([".preg_quote($validation_kigou, '/')."])/mu", $_POST[$name])){
          addErrorMessage($name,$arr[1]);
        } else if(preg_match("/[".preg_quote($validation_number, '/')."]/mu", $_POST[$name], $m)){
          addErrorMessage($name,$arr[1]);
        }
        break;
      case 'B':
        if(!preg_match("/^[\w!#$%&'*+\/=?^_{}\\|~\.\-@]+$/mu", $_POST[$name])){
          addErrorMessage($name,$arr[1]);
        }
        break;
      case 'C':
        if(!preg_match("/^[".preg_quote($validation_number, '/')."]+$/mu", $_POST[$name], $m)){
          addErrorMessage($name,$arr[1]);
        }
        break;
      case 'D':
        if(preg_match("/([".preg_quote($validation_kigou, '/')."])/mu", $_POST[$name])){
          addErrorMessage($name,$arr[1]);
        } else if(preg_match("/[".preg_quote($validation_number, '/')."]/mu", $_POST[$name], $m)){
          addErrorMessage($name,$arr[1]);
        } else if(preg_match("/[".preg_quote("ぁ-ゞ", '/')."]/mu", $_POST[$name])){
          addErrorMessage($name,$arr[1]);
        } else if(preg_match("/[".preg_quote("a-zA-Z", '/')."]/mu", $_POST[$name])){
          addErrorMessage($name,$arr[1]);
        }
        break;
      }
    }
  }
  foreach($match as $name => $arr){
    if(isset($_POST[$name]) && !myempty($_POST[$name])){
      if($_POST[$name] != $_POST[$arr[0]]){
        addErrorMessage($arr[0],$arr[1]);
      }
    }
  }
}


function proc_min()
{
  global $min_input;
  foreach($min_input as $name => $arr){
    if(isset($_POST[$name]) && !myempty($_POST[$name])){
      if(is_array($_POST[$name])){
        $len = count($_POST[$name]);
      } else {
        $len = mb_strlen($_POST[$name],'UTF-8');
      }
      if($len < $arr[0]){
        addErrorMessage($name,$arr[1]);
      }
    }
  }
}


function proc_max()
{
  global $max_input;
  foreach($max_input as $name => $arr){
    if(isset($_POST[$name]) && !myempty($_POST[$name])){
      if(is_array($_POST[$name])){
        $len = count($_POST[$name]);
      } else {
        $len = mb_strlen($_POST[$name],'UTF-8');
      }
      if($len > $arr[0]){
        addErrorMessage($name,$arr[1]);
      }
    }
  }
}


function proc_must()
{
  global $must;
  foreach($must as $name => $errmes){
    if(!isset($_POST[$name]) || myempty($_POST[$name])){
      addErrorMessage($name,$errmes);
    }
  }
}

function myempty($v){
  if(strlen($v)==0) return true;
  return false;
}

function proc_input_filter()
{
  global $input_filter;
/*
A: スペース削除
B: 全角→半角
C: 半角→全角
D: ハイフン削除
*/
  foreach($input_filter as $name => $f){
    if(!isset($_POST[$name])) continue;
    $v = $_POST[$name];
    for($i=0;$i<strlen($f);$i++){
      switch($f[$i]){
      case 'A':
        proc_filter_a($v);
        break;
      case 'B':
        proc_filter_b($v);
        break;
      case 'C':
        proc_filter_c($v);
        break;
      case 'D':
        proc_filter_d($v);
        break;
      }
    }
    $_POST[$name] = $v;
  }
}

function proc_filter_a(&$v, $k=null)
{
  if(is_array($v)){
    proc_filter_a($v,$k);
  } else {
    $v = preg_replace('/\s|　/mu',"",$v);
  }
}

function proc_filter_b(&$v, $k=null)
{
  if(is_array($v)){
    proc_filter_b($v,$k);
  } else {
    $v = mb_convert_kana($v, "a", 'UTF-8');
  }
}

function proc_filter_c(&$v, $k=null)
{
  if(is_array($v)){
    proc_filter_c($v,$k);
  } else {
    $v = mb_convert_kana($v, "AKV", 'UTF-8');
  }
}

function proc_filter_d(&$v, $k=null)
{
  if(is_array($v)){
    proc_filter_d($v,$k);
  } else {
    $v = preg_replace("/-|ー|－|―|‐/mu","",$v);
  }
}








































//////////////////////////////////////////////////////////////////////////////
//
// EFO用ライブラリファイル
//
// @charset utf-8
// @author tsuji@syspla.net
//
///////////////////////////////////////////////////////////////////////////////
