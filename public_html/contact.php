<?php
/*** 入力画面 ***/
$access_check = true;
require('libefo.php');

if(isset($_SESSION['postval']['done']) && $_SESSION['postval']['done']===true){
  $_SESSION['postval'] = array();
}
if(@$_GET['mode'] != 'back' && @$_GET['mode'] != 'error'){
  $_SESSION['postval'] = array();
}
if(@$_GET['mode'] != 'error'){
  $_SESSION['errors'] = array();
}
//var_dump($_SESSION);

?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>お問い合わせ｜コーディング代行なら、匠コーディング</title>
    <meta name="viewport" content="width=940px, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="コーディング代行なら匠コーディング">
    <meta property="og:description" content="">
    <link rel="stylesheet" href="css/normalize.css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/exvalidation.css">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/smoothScroll.js"></script>
    <script src="js/exvalidation.js"></script>
    <script src="js/exchecker-ja.js"></script>
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
      jQuery(document).ready(function($) {
        $("form").validation({
          errTipPos : "right" // left
        });
      });
      $(function() {
        $('.consentBtn  ').attr('disabled', 'disabled');

        $('#consentcheck').click(function() {
          if ($(this).prop('checked') == false) {
            $('.consentBtn  ').attr('disabled', 'disabled');
          } else {
            $('.consentBtn  ').removeAttr('disabled');
        }
      });
    });
    </script>
  </head>
  <body id="top" class="lower">
    <div class="wrapper">
      <header>
        <div class="contents_inner cf">
          <h1 class="fl-l hLogo">
            <img src="images/takumi_logo_img.png" alt="匠コーディング"><br>
            <span>コーディングの外注・代行（HTML5・Javascript・レスポンシブWebデザイン・WordPressのCMS構築）はTOHOKU IT FACTORYへ</span>
          </h1>
          <div class="Inquiry cf pcOnly">
            <a href="contact.html"><p class="fl-r"><img src="images/contact_header_img.png" alt="お問い合わせ・お見積りはこちら"></p></a>
          </div>
          <div class="spOnly">
            <div class="menuButton"><span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span></div>
          </div>
        </div>
        <nav class="spNav">
          <ul>
            <li><a href="index.html">HOME</a></li><!--
          --><li><a href="index.html#promise">選ばれる理由</a></li><!--
          --><li><a href="index.html#price">メニュー・価格表</a></li><!--
          --><li><a href="index.html#step">ご利用の流れ</a></li><!--
          --><li><a href="index.html#faq">よくあるご質問</a></li>
          </ul>
        </nav>
        <nav class="fixNav">
          <div class="contents_inner">
            <ul class="gNav fwB pcOnly">
              <li><a href="index.html">HOME</a></li><!--
            --><li><a href="index.html#promise">選ばれる理由</a></li><!--
            --><li><a href="index.html#price">メニュー・価格表</a></li><!--
            --><li><a href="index.html#step">ご利用の流れ</a></li><!--
            --><li><a href="index.html#faq">よくあるご質問</a></li>
            </ul>
          </div>
        </nav>
      </header>
      <div class="mainContents">
        <div id="keyVisual">
          <h1 class="ttl2">お問い合わせ</h1>
        </div><!-- #keyVisual -->
        <div class="contents_inner">
          <ul class="breadcrumb">
            <li class="colorRed"><a href="index.html">HOME</a></li>
            <li>&gt;お問い合わせ</li>
          </ul>
          <div class="contactFlow">
            <p><img src="images/contact_flow01.png" alt="情報の入力"></p>
            <p>以下の項目にご入力の上、「確認画面へ」ボタンを押してください。<img src="images/contact_must01.png" alt="必須項目">は必ずご入力ください。</p>
          </div>
          <h2 class="ttl5">発注内容をお聞かせください。</h2>
          <form action="confirm.php" method="post" name="form1">
            <table class="contTable">
              <tr>
                <th><label for="page"><img src="images/contact_must02.png" alt="必須項目"><br>依頼ページ数</label></th>
                <td id="page page_non" class="chkselect">
                  <input type="text" name="page" id="page" class="wid10 chknumonly chkrequired">ページ<input type="checkbox" name="page_non" value="未定">未定
                  <ul class="antList">
                  <li>通常、縦3,000px・までを1ページとして計算。<br>
                  3,000pxを超える場合は1,000pxごと2,500円ご請求致します。</li>
                </ul></td>
              </tr>
              <tr>
                <th><label for="year"><img src="images/contact_must02.png" alt="必須項目"><br>ご希望納期</label></th>
                <td id="date_non" class=" chkselect">
                  <input type="text" name="year" value="<?php echoVal('year') ?>" class="wid10 chknumonly chkrequired" id="year">年<input type="text" name="month" value="<?php echoVal('month') ?>" class="wid5 chknumonly chkrequired" id="month">月<input type="text" name="day" value="<?php echoVal('day') ?>" class="wid5 chknumonly" id="day">日<input type="checkbox" name="date_non" value="未定">未定
                </td>
              </tr>
              <tr>
                <th><label for="design">参考デザインデータ</label></th>
                <td>
                  <input type="file" name="design" value="<?php echoVal('design') ?>" id="design" class="chkfileimage chkfilepfd"><br>
                アップロードファイルの種類（jpg/gif/png/zip）。最大20Mです。
              </td>
              </tr>
              <tr>
                <th><label for="detail"><img src="images/contact_must02.png" alt="必須項目"><br>発注内容<br>（ご質問などお気軽にどうぞ）</label></th>
                <td>
                  <textarea name="detail" rows="10" cols="40" class="wid100 wid50 chkrequired" value="<?php echoVal('detail') ?>" id="detail"></textarea>
                </td>
              </tr>
              <tr>
                <th><label for="rwd"><img src="images/contact_must02.png" alt="必須項目"><br>レスポンシブ対応</label></th>
                <td id="rwd" class="chkradio">
                  <input type="radio" name="rwd" value="あり" checked="checked" id="rwd">あり
                  <input type="radio" name="rwd" value="なし">なし
                  <input type="radio" name="rwd" value="未定">未定
                </td>
              </tr>
              <tr>
                <th><label for="js"><img src="images/contact_must02.png" alt="必須項目"><br>JacaScriptの使用</label></th>
                <td id="js" class="chkradio">
                  <input type="radio" name="js" value="あり" checked="checked" id="js">あり
                  <input type="radio" name="js" value="なし">なし
                  <input type="radio" name="js" value="未定">未定
                </td>
              </tr>
              <tr>
                <th><label for="cms"><img src="images/contact_must02.png" alt="必須項目"><br>CMSの使用</label></th>
                <td id="cms" class="chkradio">
                  <input type="radio" name="cms" value="あり" checked="checked" id="cms">あり
                  <input type="radio" name="cms" value="なし">なし
                  <input type="radio" name="cms" value="未定">未定
                </td>
              </tr>
              <tr>
                <th><label for="print">印刷対応</label></th>
                <td id="print" class="chkradio">
                  <input type="radio" name="print" value="あり" id="print">あり
                  <input type="radio" name="print" value="なし">なし
                  <input type="radio" name="print" value="未定" checked="checked">未定
                </td>
              </tr>
              <tr>
                <th><label for="other">その他</label></th>
                <td>
                  <textarea name="other" rows="10" cols="40" class="wid100" id="other" value="<?php echoVal('other') ?>"></textarea>
                </td>
              </tr>
              <tr>
                <th><label for="name"><img src="images/contact_must02.png" alt="必須項目"><br>お名前</label></th>
                <td>
                  <input type="text" name="name" value="<?php echoVal('name') ?>" class="wid50 " id="name">
                </td>
              </tr>
              <tr>
                <th><label for="kana"><img src="images/contact_must02.png" alt="必須項目"><br>お名前（フリガナ）</label></th>
                <td>
                  <input type="text" name="kana" value="<?php echoVal('kana') ?>" class="wid50  chkkatakana" id="kana">
                </td>
              </tr>
              <tr>
                <th><label for="companyName">会社名・組織名</label></th>
                <td>
                  <input type="text" name="companyName" value="<?php echoVal('companyName') ?>" class="wid95" id="companyName">
                </td>
              </tr>
              <tr>
                <th><label for="address">ご住所</label></th>
                <td>
                  〒<input type="text" name="zip" value="<?php echoVal('code') ?>" id="address" class="chkzip"><input type="button" class="zipBtn" onclick="AjaxZip3.zip2addr('zip','','pref','town');" value="住所検索"><br>
                  都道府県<input type="text" name="pref" value="<?php echoVal('pref') ?>"><br>
                  市区町村番地<input type="text" name="town" value="<?php echoVal('town') ?>" class="wid70"><br>
                  マンション・ビル名<input type="text" name="buil" value="<?php echoVal('buil') ?>" class="wid70">
                </td>
              </tr>
              <tr>
                <th><label for="tel"><img src="images/contact_must02.png" alt="必須項目"><br>お電話番号</label></th>
                <td>
                  <input type="text" name="code" value="<?php echoVal('tel') ?>" class="wid50  chkmin3 chkmax20 chktel" id="tel">
                </td>
              </tr>
              <tr>
                <th><label for="email"><img src="images/contact_must02.png" alt="必須項目"><br>メールアドレス</label></th>
                <td>
                  <input type="text" name="email" value="<?php echoVal('mail') ?>" class="wid95  chkemail chkmax256" id="email">
                </td>
              </tr>
            </table>
            <div class="consent">
              <dl>
                <dt class="colorRed">ウェブサイト上の個人情報の取扱いについて</dt>
                <dd>いただいた個人情報は、お問い合わせへの対応のみ使用し、他の目的に利用することはございません。また、弊社はあらかじめお客様の同意を得ないで、いただいた個人情報を第三者に提供いたしません。ただし、　お問い合わせへの対応のために、商品の販売元等へ提供する場合がございます。弊社のその他の個人情報に関する取扱いについては<a href="privacy.html" target="_blank">「個人情報保護に関して」</a>をご覧ください。上記事項をご確認の上、ご同意いただける方は下の「同意する」をチェックしてください。</dd>
              </dl>
              <p><input type="checkbox" name="consent" value="同意します" id="consentcheck"><label for="consentcheck">個人情報保護に関して同意する</label></p>
              <p>
                <button type="submit" name="button" class="consentBtn">確認画面へ</button>
              </p>
            </div>
          </form>
        </div>
      </div>
      <!-- /.mainContents -->
        <div class="toTop">
          <div class="contents_inner cf">
            <div class="fl-r">
              <a href="#top"><img src="images/totop_img.png" alt="totop">ページの先頭へ戻る</a>
            </div>
          </div>
        </div>
        <div id="company">
          <div class="contents_inner">
            <div class="row">
              <div class="col">
                <address>
                  <dl>
                    <dt class="txt1">株式会社TOHOKU IT FACTORY</dt>
                    <dd class="tel"><img src="images/tel_img.png" alt="">022-281-8378</dd>
                    <dd class="txt3">[受付時間] 平日09:00～19:00（土日・祝日除く）</dd>
                  </dl>
                </address>
              </div>
              <div class="col">
                <p class="txtAC"><a href="contact.html"><img src="images/contact_foot_img.png" alt="contact"></a></p>
              </div>
            </div>
          </div>
        </div>
        <div id="privacy">
          <div class="contents_inner cf">
            <ul class="fl-l">
              <li><span class="colorRed fwB">&gt;</span>プライバシーポリシー</li>
              <!-- <li><a href="#"><span class="colorRed fwB">&gt;</span>利用規約</a></li> -->
              <!-- <li><a href="#"><span class="colorRed fwB">&gt;</span>会社概要</a></li> -->
            </ul>
            <p class="txtAR"><a href="https://www.facebook.com/%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BETOHOKU-IT-FACTORY-1225876704106896/" target="_blank"><img src="images/privacy_fb.png" alt="facebook"></a></p>
          </div>
        </div>
      <footer>
        <div class="contents_inner">
          <nav class="pcOnly">
            <ul class="fNav">
              <li><a href="index.html">HOME</a></li>
              <li><a href="index.html#promise">選ばれる理由</a></li>
              <li><a href="index.html#price">メニュー・価格表</a></li>
              <li><a href="index.html#step">ご利用の流れ</a></li>
              <li><a href="index.html#faq">よくあるご質問</a></li>
              <li><a href="contact.html">お問い合わせ・お見積り</a></li>
              <!-- <li>プライバシーポリシー</li> -->
              <!-- <li><a href="#">利用規約</a></li> -->
              <!-- <li><a href="#">会社概要</a></li> -->
            </ul>
          </nav>
          <h1>  コーディングの外注・代行（HTML5・Javascript・レスポンシブWebデザイン・WordPressのCMS構築）はTOHOKU IT FACTORYへ</h1>
          <div class="row">
            <div class="col">
              <dl class="address">
                <dt>株式会社TOHOKU IT FACTORY</dt>
                <dd>〒980-0811</dd>
                <dd>宮城県仙台市青葉区一番町2丁目5番5号 東中央ビル（A棟）5F</dd>
                <dd>TEL:022-281-8378（代表）FAX:020-4664-1408</dd>
              </dl>
            </div>
            <div class="col">
              <p class="txtAC copy"><small>COPYRIGHT(C) TOHOKU IT FACTORY inc. ALL RIGHTS RESERVED.</small></p>
            </div>
          </div>
        </div>
      </footer>
    </div><!-- /.wrapper -->
  </body>
</html>
