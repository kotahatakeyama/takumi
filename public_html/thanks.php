<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>受付完了｜コーディング代行なら、匠コーディング</title>
    <meta name="viewport" content="width=940px, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="コーディング代行なら匠コーディング">
    <meta property="og:description" content="">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/smoothScroll.js"></script>
  </head>
  <body id="top" class="lower">
    <div class="wrapper">
      <header>
        <div class="contents_inner cf">
          <h1 class="fl-l hLogo">
            <img src="images/takumi_logo_img.png" alt="匠コーディング"><br>
            <span>コーディングの外注・代行（HTML5・Javascript・レスポンシブWebデザイン・WordPressのCMS構築）はTOHOKU IT FACTORYへ</span>
          </h1>
          <div class="Inquiry cf pcOnly">
            <a href="contact.php"><p class="fl-r"><img src="images/contact_header_img.png" alt="お問い合わせ・お見積りはこちら"></p></a>
          </div>
          <div class="spOnly">
            <div class="menuButton"><span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span></div>
          </div>
        </div>
        <nav class="spNav">
          <ul>
            <li><a href="index.html">HOME</a></li><!--
          --><li><a href="index.html#promise">選ばれる理由</a></li><!--
          --><li><a href="index.html#price">メニュー・価格表</a></li><!--
          --><li><a href="index.html#step">ご利用の流れ</a></li><!--
          --><li><a href="index.html#faq">よくあるご質問</a></li>
          </ul>
        </nav>
        <nav class="fixNav">
          <div class="contents_inner">
            <ul class="gNav fwB pcOnly">
              <li><a href="index.html">HOME</a></li><!--
            --><li><a href="index.html#promise">選ばれる理由</a></li><!--
            --><li><a href="index.html#price">メニュー・価格表</a></li><!--
            --><li><a href="index.html#step">ご利用の流れ</a></li><!--
            --><li><a href="index.html#faq">よくあるご質問</a></li>
            </ul>
          </div>
        </nav>
      </header>
      <div class="mainContents">
        <div id="keyVisual">
          <h1 class="ttl2">お問い合わせ</h1>
        </div><!-- #keyVisual -->
        <div class="contents_inner">
          <ul class="breadcrumb">
            <li class="colorRed"><a href="index.html">HOME</a></li>
            <li>&gt;お問い合わせ</li>
          </ul>
          <div class="contactFlow">
            <p><img src="images/contact_flow03.png" alt="受付完了"></p>
          </div>
          <div class="message">
            <p class="txtAC"><img src="images/thanks_finish.png" alt=""></p>
            <h2 class="ttl3 txtAC">お問い合わせを受け付けました。</h2>
            <p class="txtAC">お問い合わせ有難うございました。<br>
            後日、担当者から連絡させていただきますので、今しばらくお待ちください。</p>
          </div>
          <div class="backLink">
            <p class="txtAC fwB"><a href="index.html">TOPに戻る</a></p>
          </div>
      </div>
      <!-- /.mainContents -->
        <div class="toTop">
          <div class="contents_inner cf">
            <div class="fl-r">
              <a href="#top"><img src="images/totop_img.png" alt="totop">ページの先頭へ戻る</a>
            </div>
          </div>
        </div>
        <div id="company">
          <div class="contents_inner">
            <div class="row">
              <div class="col">
                <address>
                  <dl>
                    <dt class="txt1">株式会社TOHOKU IT FACTORY</dt>
                    <dd class="tel"><img src="images/tel_img.png" alt="">022-281-8378</dd>
                    <dd class="txt3">[受付時間] 平日09:00～19:00（土日・祝日除く）</dd>
                  </dl>
                </address>
              </div>
              <div class="col">
                <p class="txtAC"><a href="contact.php"><img src="images/contact_foot_img.png" alt="contact"></a></p>
              </div>
            </div>
          </div>
        </div>
        <div id="privacy">
          <div class="contents_inner cf">
            <ul class="fl-l">
              <li><span class="colorRed fwB">&gt;</span>プライバシーポリシー</li>
              <!-- <li><a href="#"><span class="colorRed fwB">&gt;</span>利用規約</a></li> -->
              <!-- <li><a href="#"><span class="colorRed fwB">&gt;</span>会社概要</a></li> -->
            </ul>
            <p class="txtAR"><a href="https://www.facebook.com/%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BETOHOKU-IT-FACTORY-1225876704106896/" target="_blank"><img src="images/privacy_fb.png" alt="facebook"></a></p>
          </div>
        </div>
      <footer>
        <div class="contents_inner">
          <nav class="pcOnly">
            <ul class="fNav">
              <li><a href="index.html">HOME</a></li>
              <li><a href="index.html#promise">選ばれる理由</a></li>
              <li><a href="index.html#price">メニュー・価格表</a></li>
              <li><a href="index.html#step">ご利用の流れ</a></li>
              <li><a href="index.html#faq">よくあるご質問</a></li>
              <li><a href="contact.php">お問い合わせ・お見積り</a></li>
              <li>プライバシーポリシー</li>
              <!-- <li><a href="#">利用規約</a></li> -->
              <!-- <li><a href="#">会社概要</a></li> -->
            </ul>
          </nav>
          <h1>  コーディングの外注・代行（HTML5・Javascript・レスポンシブWebデザイン・WordPressのCMS構築）はTOHOKU IT FACTORYへ</h1>
          <div class="row">
            <div class="col">
              <dl class="address">
                <dt>株式会社TOHOKU IT FACTORY</dt>
                <dd>〒980-0811</dd>
                <dd>宮城県仙台市青葉区一番町2丁目5番5号 東中央ビル（A棟）5F</dd>
                <dd>TEL:022-281-8378（代表）FAX:020-4664-1408</dd>
              </dl>
            </div>
            <div class="col">
              <p class="txtAC copy"><small>COPYRIGHT(C) TOHOKU IT FACTORY inc. ALL RIGHTS RESERVED.</small></p>
            </div>
          </div>
        </div>
      </footer>
    </div><!-- /.wrapper -->
  </body>
</html>
