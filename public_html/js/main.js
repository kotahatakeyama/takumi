$(document).ready(function(){$("a").bind("focus",function(){if(this.blur)this.blur();});});
//スライドで付いてくるメニューバー設定
$(function($) {
   var nav = $('nav.fixNav'),
   offset = nav.offset();

    $(window).scroll(function () {
      if($(window).scrollTop() > offset.top  ) {
        nav.addClass('fixed');
      } else {
        nav.removeClass('fixed');
    }
  });
});

//ドロップダウンメニュー
$(function() {
    $(".spNav").css("display","none");
    $(".menuButton").on("click", function() {
        $(".spNav").slideToggle();
    });
});

$(function() {
    function accordion() {
        $(this).toggleClass("active").next().slideToggle(300);
    }
    $(".accordion .toggle").click(accordion);
});
